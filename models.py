
class User:
    username = None
    id = None
    full_name = None
    profile_picture = None
    website = None
    counts = {"follows": 0, "followed_by": 0, "media": 0}
    followed_by = []
