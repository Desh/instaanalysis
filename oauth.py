import requests
from bs4 import BeautifulSoup
from selenium import webdriver

from config import *


SITE_API = "https://api.instagram.com"


class OAuth2Exception(Exception):
    def __init__(self, description):
        self.description = description

    def __str__(self):
        return self.description


class OAuth2:
    def __init__(self, username, password, scope=["basic"]):
        self.username = username
        self.password = password
        self.scope = scope
        self.oauth_url = "/oauth/authorize/" \
                          "?client_id={}&redirect_uri={}" \
                          "&response_type=code&scope={}".format(CLIENT_ID, REDIRECT_URI, '+'.join(scope))

    def get_code_redirect_url(self):
        session = requests.session()
        session.headers.update({"Host": "www.instagram.com",
                                "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:47.0) Gecko/20100101 Firefox/47.0",
                                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                                "Accept-Encoding": "gzip, deflate, br"})
        response = session.get(SITE_API + self.oauth_url)
        if response.status_code != 200:
            raise OAuth2Exception("Something went wrong")
        return response.url


        """

        ####First attempt with request library####
        Gets 403 error, problem with instagram cookies


        response = session.get(response.url)
        session.cookies.update(response.cookies)
        print(response.url)
        session.headers.update({"Referer": SITE_API + self.oauth_url})
        soup = BeautifulSoup(response.text, "html.parser")
        csfr = soup.find("input", {"name": "csrfmiddlewaretoken"}).get("value")
        response = session.post(response.url,
                                data={"username": self.username,
                                      "password": self.password,
                                      "csrfmiddlewaretoken": csfr})
        """


        """

        #### Second attempt with Selenium ####
        Doesn't work on MacOs

        driver = webdriver.Firefox()
        driver.get(response.url)
        elem = driver.find_element_by_id("id_username")
        elem.send_keys(self.username)
        elem = driver.find_element_by_id("id_password")
        elem.send_keys(self.password)
        option = driver.find_element_by_class_name("button-green")
        option.click()
        """

    def get_access_token_with_code(self, code):
        client_params = {
            "client_id": CLIENT_ID,
            "client_secret": CLIENT_SECRET,
            "redirect_uri": REDIRECT_URI,
            "grant_type": "authorization_code",
            "code": code
        }
        response = requests.post("https://api.instagram.com/oauth/access_token", data=client_params)
        if response.status_code != 200:
            raise OAuth2Exception(response.text)
        return response.json()["access_token"]
