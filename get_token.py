import oauth

username = input("Type username: ").strip()
password = input("Type password: ").strip()
print("Categories: 'basic', 'public_content', 'follower_list', 'comments', 'likes', 'relationships'")
scope = input("Type scope categories").strip().split(" ")
if scope == ['']:
    scope = ['basic', 'public_content', 'follower_list', 'comments', 'likes', 'relationships']
client = oauth.OAuth2(username=username, password=password, scope=scope)
code = input("Visit url and paste 'code' from redirect href: " + client.get_code_redirect_url() + '\n code: ').strip()
print("Your access_token: " + client.get_access_token_with_code(code))


# scope = ['basic', 'public_content', 'follower_list', 'comments', 'likes', 'relationships']
# client = oauth.OAuth2(username="", password="", scope=scope)
# client.get_code_redirect_url()

