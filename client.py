import requests
import models, oauth
from config import *


SITE_API = "https://api.instagram.com/"
API_VER = "v1"


class APImethods:
    def __init__(self, username, password, access_token, scope=['basic']):
        self.username = username
        self.password = password
        self.scope = scope
        self.access_token = access_token

    def get_user_self(self):
        response = requests.get(SITE_API + API_VER +
                                "/users/self/?access_token={}".format(self.access_token))
        if response.status_code != 200:
            raise oauth.OAuth2Exception(response.json()['meta']['error_message'])
        re_data = response.json()['data']
        user = models.User()
        user.username = re_data['username']
        user.id = re_data['id']
        user.full_name = re_data['full_name']
        user.profile_picture = re_data['profile_picture']
        user.website = re_data['website']
        user.counts['follows'] = re_data['counts']['follows']
        user.counts['followed_by'] = re_data['counts']['followed_by']
        user.counts['media'] = re_data['counts']['media']
        return user

    def get_user_follows(self):
        response = requests.get(SITE_API + API_VER +
                                "/users/self/follows?access_token={}".format(self.access_token))
        if response.status_code != 200:
            raise oauth.OAuth2Exception(response.json()['meta']['error_message'])
        re_data = response.json()['data']
        followers = []
        for el in re_data:
            user = models.User()
            user.username = el['username']
            user.id = el['id']
            user.full_name = el['full_name']
            user.profile_picture = el['profile_picture']
            followers.append(user)
        return followers

    def get_resent_by_id(self, user_id, count):
        response = requests.get(SITE_API + API_VER +
                                "/users/{}/media/recent/?access_token={}&count={}".format(
                                    user_id, self.access_token, count))
        if response.status_code != 200:
            raise oauth.OAuth2Exception(response.json()['meta']['error_message'])
        re_data = response.json()['data']
        print(re_data)
        return re_data

if __name__ == "__main__":
    api = APImethods(USERNAME, PASSWORD, access_token=ACCESS_TOKEN,
                     scope=['basic', 'public_content', 'follower_list', 'comments', 'likes', 'relationships'])
    user = api.get_user_self()
    followers = api.get_user_follows()
    media = api.get_resent_by_id(followers[2].id, 4)

